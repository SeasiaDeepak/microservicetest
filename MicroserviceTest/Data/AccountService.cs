﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MicroserviceTest.Collections;
using MicroserviceTest.Models;
using MongoDB.Driver;

namespace MicroserviceTest.Data
{
    public class AccountService : IAccountService
    {
        DbContext _db;
        public AccountService(DbContext db)
        {
            _db = db;
        }
        public async Task<bool> InsertUser(UserViewModel userDetails)
        {
            try
            {
                var user = new User()
                {
                    Id = Guid.NewGuid().ToString(),
                    FirstName = userDetails.FirstName,
                    LastName = userDetails.LastName,
                    Email = userDetails.Email,
                    PhoneNumber = userDetails.PhoneNumber,
                    IsActive = true,
                    CreatedOn = DateTime.Now,
                    ModifiedOn = DateTime.Now
                };
                await _db.Users.InsertOneAsync(user);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<ICollection<User>> GetAllUsers()
        {
            return await _db.Users.Find(_ => true).ToListAsync();
        }

        public async Task<User> GetUserDetailsById(string id)
        {
            return await _db.Users.Find(x => x.Id == id).FirstOrDefaultAsync();
        }
        public async Task<bool> UpdateUser(UserViewModel userDetails)
        {
            try
            {
                var userObject = _db.Users.Find(x => x.Id == userDetails.Id).FirstOrDefault();
                if (userObject != null)
                {
                    var updates = new List<UpdateDefinition<User>>();
                    updates.Add(Builders<User>.Update.Set(x => x.FirstName, userDetails.FirstName));
                    updates.Add(Builders<User>.Update.Set(x => x.LastName, userDetails.LastName));
                    updates.Add(Builders<User>.Update.Set(x => x.Email, userDetails.Email));
                    updates.Add(Builders<User>.Update.Set(x => x.ModifiedOn, DateTime.Now));                    
                    await _db.Users.FindOneAndUpdateAsync(Builders<User>.Filter.Eq(x => x.Id, userDetails.Id),
                        Builders<User>.Update.Combine(updates),
                        new FindOneAndUpdateOptions<User, User> { ReturnDocument = ReturnDocument.After });

                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> UpdateStatus(string id, bool status)
        {
            try
            {
                var userObject = _db.Users.Find(x => x.Id == id).FirstOrDefault();
                if (userObject != null)
                {
                    var updates = new List<UpdateDefinition<User>>();
                    updates.Add(Builders<User>.Update.Set(x => x.IsActive, status));
                    await _db.Users.FindOneAndUpdateAsync(Builders<User>.Filter.Eq(x => x.Id, id),
                        Builders<User>.Update.Combine(updates),
                        new FindOneAndUpdateOptions<User, User> { ReturnDocument = ReturnDocument.After });
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<bool> DeleteUser(string id)
        {
            await _db.Users.DeleteOneAsync(x => x.Id == id);
            return true;
        }
    }
}

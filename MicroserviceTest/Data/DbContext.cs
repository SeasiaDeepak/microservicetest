﻿using MicroserviceTest.Collections;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MicroserviceTest.Data
{
    //public class DbContext
    //{
    //}

    public class DbContext
    {

        /// <summary>
        /// The MongoDB client object used to communicate with the MongoDB server
        /// </summary>
        private MongoClient _client;
        /// <summary>
        /// The Database object linked to the MongoDB client
        /// </summary>
        private IMongoDatabase _db;
        /// <summary>
        /// Default constructor
        /// </summary>
        public DbContext()
        {

            try
            {
                string mongo_host = Environment.GetEnvironmentVariable("MONGODB_CONNECTION") ?? "localhost";
                _client = new MongoClient($"mongodb://{mongo_host}:27017");
                _db = _client.GetDatabase("TestUserDB");
                var state = _client.Cluster.Description.State;
            }
            catch (Exception ex)
            {
                throw new Exception("Can not access to db server.", ex);
            }
        }

        public IMongoCollection<User> Users
        {
            get
            {
                return _db.GetCollection<User>("User");
            }
        }
    }
}

﻿using MicroserviceTest.Collections;
using MicroserviceTest.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MicroserviceTest.Data
{
    public interface IAccountService
    {
        Task<bool> InsertUser(UserViewModel userDetails);
        Task<ICollection<User>> GetAllUsers();
        Task<User> GetUserDetailsById(string id);
        Task<bool> UpdateUser(UserViewModel userDetails);
        Task<bool> UpdateStatus(string id, bool status);
        Task<bool> DeleteUser(string id);
    }
}

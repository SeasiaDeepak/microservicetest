﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using MicroserviceTest.Collections;
using MicroserviceTest.Data;
using MicroserviceTest.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MicroserviceTest.Controllers
{
    public class AccountController : Controller
    {
        private readonly IAccountService _service;
        public AccountController(IAccountService service)
        {
            _service = service;
        }

        [HttpPost]
        [Route("api/Account/CreateUser")]
        public async Task<ActionResult> InsertUser([FromBody]UserViewModel userDetails)
        {
            try
            {
                if (!string.IsNullOrEmpty(userDetails.Email) && !string.IsNullOrEmpty(userDetails.FirstName) && !string.IsNullOrEmpty(userDetails.LastName))
                {
                    bool isEmail = Regex.IsMatch(userDetails.Email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
                    if (!isEmail)
                    {
                        return Json(new { statusCode = HttpStatusCode.BadRequest, Message = "Please Provide a Valid Email", Data = new { } });
                    }

                    var response = await _service.InsertUser(userDetails);
                    if (response)
                    {
                        return Json(new { statusCode = HttpStatusCode.OK, Message = "User Created", Data = new { } });
                    }
                    else
                    {
                        return Json(new { statusCode = HttpStatusCode.BadRequest, Message = "Unable to create user.", Data = new { } });
                    }
                }
                else
                {
                    return Json(new { statusCode = HttpStatusCode.BadRequest, Message = "Email, FirstName, LastName fields are required.", Data = new { } });
                }
            }
            catch (Exception ex)
            {
                return Json(new { statusCode = HttpStatusCode.BadRequest, Message = "Something went wrong.", Data = new { } });
            }
        }

        [Route("api/Account/GetAllUsers")]
        public async Task<ActionResult> GetAll()
        {
            try
            {
                var response = await _service.GetAllUsers();
                return Json(new { statusCode = HttpStatusCode.OK, Message = "Users Lists", Data = response });
            }
            catch (Exception ex)
            {
                return Json(new { statusCode = HttpStatusCode.BadRequest, Message = "Something went wrong.", Data = new { } });
            }
        }

        [HttpGet]
        [Route("api/Account/GetUserDetailsById")]
        public async Task<ActionResult> GetById(string userId)
        {
            try
            {
                if (string.IsNullOrEmpty(userId))
                    return Json(new { statusCode = HttpStatusCode.BadRequest, Message = "UserId is required", Data = new { } });

                var response = await _service.GetUserDetailsById(userId);
                return Json(new { statusCode = HttpStatusCode.OK, Message = "Users Lists", Data = response });
            }
            catch (Exception ex)
            {
                return Json(new { statusCode = HttpStatusCode.BadRequest, Message = "Something went wrong.", Data = new { } });
            }
        }

        [HttpPost]
        [Route("api/Account/UpdateUser")]
        public async Task<ActionResult> Update([FromBody]UserViewModel userDetails)
        {
            try
            {
                if (string.IsNullOrEmpty(userDetails.Id))
                    return Json(new { statusCode = HttpStatusCode.BadRequest, Message = "UserId is required", Data = new { } });

                if (!string.IsNullOrEmpty(userDetails.Email) && !string.IsNullOrEmpty(userDetails.FirstName) && !string.IsNullOrEmpty(userDetails.LastName))
                {
                    bool isEmail = Regex.IsMatch(userDetails.Email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
                    if (!isEmail)
                        return Json(new { statusCode = HttpStatusCode.BadRequest, Message = "Please Provide a Valid Email", Data = new { } });

                    var response = await _service.UpdateUser(userDetails);
                    if (response)
                    {
                        return Json(new { statusCode = HttpStatusCode.OK, Message = "User updated successfully", Data = new { } });
                    }
                    else
                    {
                        return Json(new { statusCode = HttpStatusCode.BadRequest, Message = "No user found corresponding to the given details", Data = new { } });
                    }
                }
                else
                {
                    return Json(new { statusCode = HttpStatusCode.BadRequest, Message = "Email, FirstName, LastName fields are required.", Data = new { } });
                }
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    statusCode = HttpStatusCode.BadRequest,
                    Message = "Something went wrong.",
                    Data = new { }
                });
            }
        }

        [HttpPost]
        [Route("api/Account/ChangeStatus")]
        public async Task<ActionResult> ChangeStatus([FromBody]UserViewModel userDetails)
        {
            try
            {
                if (string.IsNullOrEmpty(userDetails.Id))
                    return Json(new { statusCode = HttpStatusCode.BadRequest, Message = "UserId is required", Data = new { } });

                var response = await _service.UpdateStatus(userDetails.Id, userDetails.IsActive);
                if (response)
                {
                    return Json(new { statusCode = HttpStatusCode.OK, Message = "Status updated successfully", Data = new { } });
                }
                else
                {
                    return Json(new { statusCode = HttpStatusCode.BadRequest, Message = "No user found corresponding to the given user id", Data = new { } });
                }
            }
            catch (Exception ex)
            {
                return Json(new { statusCode = HttpStatusCode.BadRequest, Message = "Something went wrong.", Data = new { } });
            }
        }

        [HttpDelete]
        [Route("api/Account/DeleteUser")]
        public async Task<ActionResult> DeleteUser(string userId)
        {
            try
            {
                if (string.IsNullOrEmpty(userId))
                    return Json(new { statusCode = HttpStatusCode.BadRequest, Message = "UserId is required", Data = new { } });

                var response = await _service.DeleteUser(userId);
                if (response)
                {
                    return Json(new { statusCode = HttpStatusCode.OK, Message = "User Deleted", Data = new { } });
                }
                else
                {
                    return Json(new { statusCode = HttpStatusCode.BadRequest, Message = "Unable to delete user", Data = new { } });
                }
            }
            catch (Exception ex)
            {
                return Json(new { statusCode = HttpStatusCode.BadRequest, Message = "Something went wrong.", Data = new { } });
            }
        }
    }
}
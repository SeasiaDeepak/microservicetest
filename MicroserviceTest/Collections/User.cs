﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MicroserviceTest.Collections
{
    public class User
    {
        [BsonId]
        [BsonElement("id")]
        public string Id { get; set; }

        [BsonElement("firstname")]
        public string FirstName { get; set; }

        [BsonElement("lastname")]
        public string LastName { get; set; }

        [BsonElement("email")]
        public string Email { get; set; }

        [BsonElement("phonenumber")]
        public string PhoneNumber { get; set; }

        [BsonElement("isactive")]
        public bool IsActive { get; set; }

        [BsonElement("created-on")]
        public DateTime CreatedOn { get; set; }

        [BsonElement("modified-on")]
        public DateTime ModifiedOn { get; set; }

    }
}
